import java.util.ArrayList;
import java.util.List;

public class PrimeNumbers {

	static int upperBorder= 1000000;
	public static void main (String[] args){
		for (int i=2; i<upperBorder; i+=10000){
			System.out.println(""+i+" Dynamic:     "+measureTime(true, i)+" ns"); 
			System.out.println(""+i+" Non Dynamic: "+measureTime(false, i)+" ns");

		}	
	}

	public static long  measureTime(boolean dynamic, int value){
		long start = System.nanoTime();
		if (dynamic) dynamic(value);
		else nonDynamic (value);
		long end = System.nanoTime();
		return end-start;
	}

	public static void nonDynamic(int max){
		for (int i=2; i<=max; i++){
			boolean isPrime = true;
			for (int j=2; j<i; j++){
				if (i%j==0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime){
				//System.out.println("Found Prime:    "+i);
			}
		}
	}
	public static void dynamic (int max) {
		List<Integer> primeList = new ArrayList<>();
		for (int i = 2; i <= max; i++) {
			boolean isPrime = true;
			for (Integer j: primeList){
				if (i%j.intValue()==0){
					isPrime=false;
					break;
				}
			}	
			if(isPrime) primeList.add(new Integer(i));
		}
	}

}
