

class Fib {
	static int upperBorder= 100000;
	public static void main (String[] args){
		for (int i=2; i<upperBorder; i++){
			System.out.println(""+i+" Dynamic:     "+measureTime(true, i)+"ns");
			System.out.println(""+i+" Non Dynamic: "+measureTime(false, i)+"ns");
		}
	}

	public static long  measureTime(boolean dynamic, int value){
		long start = System.nanoTime();
		if (dynamic)fibDyn (value);
		else fibRec (value);
		long end = System.nanoTime();
		return end-start;
	}

	public static int fibRec(int i){
		if (i<2){
			return 1;
		}
	        
		return fibRec(i-1)+ fibRec (i-2);
	}
    
    
    
	public static int fibDyn (int i){
		int fibDynMem [] = new int [i];
		for (int j = 2; j<i; j++){
			fibDynMem[j] = 0;
		}
		fibDynMem [0] = 1;
		fibDynMem [1] = 1;
		return fibDynRec (fibDynMem,i);
	}
    
private static int fibDynRec(int fibDynMem [], int i){
if (fibDynMem[i-1] == 0){
fibDynMem[i-1] = fibDynRec(fibDynMem, i-1)+fibDynRec(fibDynMem, i-2);
}
return fibDynMem[i-1];
}
}        

